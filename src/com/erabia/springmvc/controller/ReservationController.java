package com.erabia.springmvc.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.erabia.springmvc.bean.Reservation;
@RequestMapping("/reservation")
@Controller
public class ReservationController {
	@RequestMapping("/bookingForm")
	public String bookingForm(Model model) {
		Reservation reservation = new Reservation();
		model.addAttribute("reservation", reservation);
		
		
		return "reservation-page";
	}
	@RequestMapping("/submitForm")
	public String submit(@ModelAttribute("reservation") Reservation reservation) {
		
		System.out.println(reservation.toString());
		return "confirmation-page";
		
	}

}
